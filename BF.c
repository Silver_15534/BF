#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
unsigned char l[99];
int main(int argc, char const *argv[])
{
	unsigned i, d;
	scanf("%d", &i);
	for (unsigned char b[i], *bp, *p = &l[49]; ; putchar('\n'))
	{
		bp = b - 1, d = 0;
		printf(">> ");
		for (scanf("%s", b); *++bp; )
		{
			switch(*bp)
			{
				case '>':
					p++;
					break;
				case '<':
					p--;
					break;
				case '+':
					*p == 255 ? *p = 0 : (*p)++;
					break;
				case '-':
					*p ? (*p)-- : (*p = 255);
					break;
				case '[':
					if (!*p)
					{
						for (; ; )
						{
							if (*++bp == ']')
							{
								if (d)
								{
									d--;
								}
								else
								{
									break;
								}
							}
							if (*bp == '[')
							{
								d++;
							}
						}
					}
					break;
				case ']':
					for (; ; )
					{
						if (*bp-- == '[')
						{
							if (d)
							{
								d--;
							}
							else
							{
								break;
							}
						}
						if (*bp == ']')
						{
							d++;
						}
					}
					break;
				case '.':
					putchar(*p);
					break;
				case ',':
					*p = getch();
					break;
				case 'c':
					system("cls");
					break;
				case 'r':
					for (i = 99; i; l[--i] = 0);
					p = &l[49];
					break;
				case 'q':
					goto q;
			}
		}
	}
	q:return 0;
}
